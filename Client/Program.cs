﻿using LINQ.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    class Program
    {
        private static ProjectInterface projectInterface;
        static void Main(string[] args)
        {
            //HttpClient httpClient = new HttpClient();

            //HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "https://localhost:44343/api/state");
            //var content = JsonConvert.SerializeObject(new State() { Id = 100, Value = "string" });
            //request.Content = new StringContent(content, Encoding.UTF8, "application/json");
            //httpClient.SendAsync(request);

            projectInterface = new ProjectInterface();

            projectInterface.Start();
        }
    }
}
