﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskProject.Model;
using TaskProject.Repositories;

namespace TaskProject.Services
{
    public class TeamService : IService<Team>
    {
        IRepository<Team> _states;
        public TeamService(IRepository<Team> states)
        {
            _states = states;
        }

        public void Create(Team entity)
        {
            entity.CreatedAt = DateTime.Now;
            _states.Create(entity);
        }

        public void Delete(Team entity)
        {
            _states.Delete(entity);
        }

        public void Delete(int id)
        {
            _states.Delete(id);
        }

        public IEnumerable<Team> GetEntities(Func<Team, bool> filter = null)
        {
            return _states.Get(filter);
        }

        public void Update(Team entity)
        {
            var state = _states.Get(x => x.Id == entity.Id).FirstOrDefault();
            if(state != null)
                entity.CreatedAt = state.CreatedAt;
            _states.Update(entity);
        }
    }
}
