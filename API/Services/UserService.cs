﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskProject.Model;
using TaskProject.Repositories;

namespace TaskProject.Services
{
    public class UserService : IService<User>
    {
        IRepository<User> _users;
        IRepository<Team> _teams;

        public UserService(IRepository<User> users, IRepository<Team> teams)
        {
            _users = users;
            _teams = teams;
        }

        public void Create(User entity)
        {
            if (entity.NewTeamId != 0)
            {
                entity.Team = _teams.Get(new Func<Team, bool>(x => x.Id == entity.NewTeamId)).FirstOrDefault();
            }

            entity.RegisteredAt = DateTime.Now;
            entity.NewTeamId = 0;
            _users.Create(entity);
        }

        public void Delete(User entity)
        {
            _users.Delete(entity);
        }

        public void Delete(int id)
        {
            _users.Delete(id);
        }

        public IEnumerable<User> GetEntities(Func<User, bool> filter = null)
        {
            return _users.Get(filter);
        }

        public void Update(User entity)
        {
            if (entity.NewTeamId != 0)
            {
                var team = _teams.Get(new Func<Team, bool>(x => x.Id == entity.NewTeamId)).FirstOrDefault();
                if(team != null)
                    entity.Team = team;
            }

            var user = _users.Get(x => x.Id == entity.Id).FirstOrDefault();
            if (user != null)
                entity.RegisteredAt = user.RegisteredAt;

            entity.NewTeamId = 0;
            _users.Update(entity);
        }
    }
}
