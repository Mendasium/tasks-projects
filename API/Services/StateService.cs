﻿using TaskProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskProject.Repositories;

namespace TaskProject.Services
{
    public class StateService : IService<State>
    {
        IRepository<State> _states;
        public StateService(IRepository<State> states)
        {
            _states = states;
        }

        public void Create(State entity)
        {
            _states.Create(entity);
        }

        public void Delete(State entity)
        {
            _states.Delete(entity);
        }

        public void Delete(int id)
        {
            _states.Delete(id);
        }

        public IEnumerable<State> GetEntities(Func<State, bool> filter = null)
        {
            return _states.Get(filter);
        }

        public void Update(State entity)
        {
            _states.Update(entity);
        }
    }
}
