﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskProject.Model
{
    public class State : Entity
    {
        public string Value { get; set; }

        public override string ToString()
        {
            return $"{Id} - {Value}";
        }

        public override void Update(Entity entity)
        {
            if (entity is State state)
            {
                if (!string.IsNullOrEmpty(state.Value) && state.Value.Length > 3 && state.Value != Value)
                    this.Value = state.Value;
            }
            else
            {
                throw new FormatException("You need to use state entity here");
            }
        }
    }
}
