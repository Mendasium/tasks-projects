﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TaskProject.Services;
using TaskProject.Model;
using AutoMapper;
using TaskProject.Model.DTOModel;
using System.Linq;

namespace TaskProject.Controllers
{
    [Route("api/[controller]")]
    public class ProjectController : ControllerBase
    {
        IService<Project> _projectsService;
        IMapper _mapper;
        public ProjectController(IService<Project> projectsService, IMapper mapper)
        {
            _mapper = mapper;
            _projectsService = projectsService;
        }

        [HttpPost]
        public void Add([FromBody]DTOProject dTOProject)
        {
            var project = _mapper.Map<DTOProject, Project>(dTOProject);
            _projectsService.Create(project);
        }

        [HttpPut]
        public void Update(DTOProject dTOProject)
        {
            var project = _mapper.Map<DTOProject, Project>(dTOProject);
            _projectsService.Update(project);
        }

        [HttpGet]
        public IEnumerable<DTOProject> GetAll()
        {
            return _mapper.Map<IEnumerable<Project>, IEnumerable<DTOProject>>(_projectsService.GetEntities());
        }

        [HttpGet("{id}")]
        public DTOProject GetById(int id)
        {
            return _mapper.Map<Project, DTOProject>(_projectsService.GetEntities(new Func<Project, bool>(x => x.Id == id)).FirstOrDefault());
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _projectsService.Delete(id);
        }

        [HttpDelete]
        public void Delete(DTOProject Project)
        {
            _projectsService.Delete(_mapper.Map<DTOProject, Project>(Project));
        }
    }
}
