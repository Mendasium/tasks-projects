﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TaskProject.Services;
using TaskProject.Model;
using AutoMapper;
using TaskProject.Model.DTOModel;
using System.Linq;

namespace TaskProject.Controllers
{
    [Route("api/[controller]")]
    public class DataController : ControllerBase
    {
        IService<State> _statesService;
        IService<Project> _projectService;
        IService<Task> _taskService;
        IService<Team> _teamService;
        IService<User> _userService;
        IMapper _mapper;
        public DataController(IService<State> statesService, IService<Project> projectService, IService<Task> taskService, IService<Team> teamService, IService<User> userService, IMapper mapper)
        {
            _mapper = mapper;
            _statesService = statesService;
            _projectService = projectService;
            _taskService = taskService;
            _teamService = teamService;
            _userService = userService;
        }

        [HttpGet("projectTasks/{id}")]
        public Dictionary<DTOProject, int> GetProjectTasksCountByUserId(int id)
        {
            var tasks = _taskService.GetEntities();

            return tasks.Where(t => t.PerformerId == id)
                .GroupBy(x => x.Project)
                .ToDictionary(x => _mapper.Map<Project, DTOProject>(x.Key), x => x.Count());
        }

        [HttpGet("shortNameTasks/{id}")]
        public IEnumerable<Task> GetShortnameTasksByPerformerIdByUserId(int id)
        {
            var tasks = _taskService.GetEntities();
            return tasks.Where(t => t.Name.Length < 45 && t.Performer.Id == id);
        }

        [HttpGet("finishedTasks/{id}/{year}")]
        public IEnumerable<(int Id, string Name)> GetFinishedTasksByUserId(int id, int year = 2019)
        {
            var projects = _taskService.GetEntities();

            return projects.Where(t => t.Performer.Id == id && t.FinishedAt.Year == year && t.State.Value.Equals("finished"))
                .Select(x => (Id: x.Id, Name: x.Name));
        }

        [HttpGet("sortedTeams")]
        public IEnumerable<(int id, string name, IEnumerable<DTOUser> userList)> GetSortedTeams()
        {//Команд в которых все учасники старше 12 лет нету
            var users = _userService.GetEntities();

            return users.GroupBy(u => u.Team)
                .Where(u => u.All(x => DateTime.Now.Year - x.Birthday.Year >= 12))
                .Select(t =>
                    (id: t.Key.Id,
                        name: t.Key.Name,
                    userList: _mapper.Map<IEnumerable<User>, IEnumerable<DTOUser>>(t.OrderByDescending(x => x.RegisteredAt)))
                    );
        }

        [HttpGet("sortedBusyUsers")]
        public IEnumerable<(DTOUser user, IEnumerable<DTOTask> tasks)> GetSortedUsersWithTasks()
        {
            var tasks = _taskService.GetEntities();

            return tasks.GroupBy(x => x.Performer.Id)
                .Select(x => (
                        Performer: _mapper.Map<User,DTOUser>(x.FirstOrDefault().Performer), 
                        Tasks: _mapper.Map<IEnumerable<Task>, IEnumerable<DTOTask>>(x.OrderByDescending(y => y.Name.Length).ToList())))
                .OrderBy(x => x.Performer.FirstName);
        }

        [HttpGet("userInfo/{id}")]
        public (DTOUser User, DTOProject LastProject, int CanceledTasksCount, DTOTask LongestTask, int LastProjectUserTasks) GetUserInfoById(int id)
        {
            var user = _userService.GetEntities(x => x.Id == id).FirstOrDefault();
            var lastProject = _projectService.GetEntities().OrderBy(x => x.CreatedAt).LastOrDefault();
            var tasks = _taskService.GetEntities(t => t.Performer.Id == id);

            return
                (
                    User: _mapper.Map<User,DTOUser>(user),
                    LastProject: _mapper.Map<Project, DTOProject>(lastProject),
                    CanceledTasksCount: tasks.Count(x => x.State.Value == "Canceled" || x.State.Value == "Started"),
                    LongestTask: _mapper.Map<Task, DTOTask>(tasks.OrderBy(x => x.FinishedAt - x.CreatedAt).FirstOrDefault()),
                    LastProjectUserTasks: tasks.Count(x => x.Project.Id == lastProject.Id)
                );
        }
        
        [HttpGet("projectInfo/{id}")]
        public (DTOProject Project, DTOTask LongestTask, DTOTask ShortestTask, int UsersCount) GetProjectInfoById(int id)
        {
            var project = _projectService.GetEntities(x => x.Id == id).FirstOrDefault();
            var projectTasks = _taskService.GetEntities(x => x.Project.Id == id);
            var users = _projectService.GetEntities();

            return
                (
                    Project: _mapper.Map<Project,DTOProject>(project),
                    LongestTask: _mapper.Map<Task, DTOTask>(projectTasks.OrderByDescending(t => t.Description).FirstOrDefault()),
                    ShortestTask: _mapper.Map<Task, DTOTask>(projectTasks.OrderBy(t => t.Name).FirstOrDefault()),
                    UsersCount: (projectTasks.Count() < 3 || project.Description.Length > 25 ?
                                                users.Where(x => x.Team.Id == project.Team.Id).Count() : -1)
                );
        }
    }
}
