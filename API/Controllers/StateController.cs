﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TaskProject.Services;
using TaskProject.Model;
using AutoMapper;
using TaskProject.Model.DTOModel;
using System.Linq;

namespace TaskProject.Controllers
{
    [Route("api/[controller]")]
    public class StateController : ControllerBase
    {
        IService<State> _statesService;
        IMapper _mapper;
        public StateController(IService<State> statesService, IMapper mapper)
        {
            _mapper = mapper;
            _statesService = statesService;
        }

        [HttpPost]
        public void Add([FromBody]DTOState dTOState)
        {
            var state = _mapper.Map<DTOState, State>(dTOState);
            _statesService.Create(state);
        }

        [HttpPut]
        public void Update([FromBody]DTOState dTOState)
        {
            var state = _mapper.Map<DTOState, State>(dTOState);
            _statesService.Update(state);
        }

        [HttpGet]
        public IEnumerable<DTOState> GetAll()
        {
            return _mapper.Map<IEnumerable<State>, IEnumerable<DTOState>>(_statesService.GetEntities());
        }

        [HttpGet("{id}")]
        public DTOState GetById(int id)
        {
            return _mapper.Map<State, DTOState>(_statesService.GetEntities(new Func<State, bool>(x => x.Id == id)).FirstOrDefault());
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _statesService.Delete(id);
        }

        [HttpDelete]
        public void Delete(DTOState state)
        {
            _statesService.Delete(_mapper.Map<DTOState, State>(state));
        }
    }
}
